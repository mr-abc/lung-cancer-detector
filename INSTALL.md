# Requirements

```
conda create -n luna16 python=3.10 -y
conda activate luna16

pip install numpy
pip install scipy
pip install matplotlib
pip install simpleitk
pip install diskcache
pip install cassandra-driver
pip install tensorboardx
pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
```