# Detection of Lung Cancer
Modified from https://github.com/deep-learning-with-pytorch/dlwpt-code.git

## Prepare data
Make a root directory for dataset, and cd to the directory
```
mkdir LUNA16
```

Unzip subset0~9.tar into LUNA16 directory
``` 
ln -s ${dataset_root}/LUNA16 ${project_root}/data/LUNA16

```
Pre-cache the dataset
```
python cls_precache.py # prepare cache for classification training
python seg_precache.py # prepare cache for segmentation training
```

## Train the model

```
python cls_train.py # train classification model
python seg_train.py # train segmentation model
```

## Resources
- [Deep Learning with PyTorch](https://livebook.manning.com/book/deep-learning-with-pytorch)
- [LUNA16 on Baidu](https://pan.baidu.com/s/1fOyXuqMxc5nr8d3YO9pb2Q?pwd=y93p)
- [pylidc](https://pylidc.github.io/index.html)
